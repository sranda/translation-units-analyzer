EXAMPLES_DIR?=.

EXAMPLES=$(foreach example,$(shell ls -d ${EXAMPLES_DIR}/*/),$(shell basename $(example)))

%.o: %.cpp
	g++ -c $< -o $@

define define_example
compile_example_$1: $(patsubst %.cpp,%.o,$(shell ls ${EXAMPLES_DIR}/$1/*.cpp))
clean_example_$1:
	rm -f ${EXAMPLES_DIR}/$1/*.o
endef

$(foreach example,$(EXAMPLES),$(eval $(call define_example,$(example))))

.PHONY: compile_examples clean_examples

compile_examples: $(foreach example,$(EXAMPLES),compile_example_$(example))
clean_examples: $(foreach example,$(EXAMPLES),clean_example_$(example))
