EXAMPLES_DIR=examples

include $(EXAMPLES_DIR)/examples.mk

$(EXAMPLES_DIR)/%: compile_example_%
	perl translation_units_analyzer.pl --print-json $@/*.o > $@/translation_units_analyzer.json

.PHONY: analyze_examples clean_example_analysis

analyze_examples: $(foreach example,$(EXAMPLES),$(EXAMPLES_DIR)/$(example))

clean_example_analysis:
	rm -f $(EXAMPLES_DIR)/*/translation_units_analyzer.json
