use strict;
use warnings;
use Switch;

my @defined_symbols_parameters = (
    "--demangle",
    "--defined-only",
    "--extern-only",
    "--print-size",
    "--line-numbers"
);

my @undefined_symbols_parameters = (
    "--demangle",
    "--undefined-only"
);

sub help_message {
    print "Analyzes dependencies among translation unit symbols.\n";
    print "Defined symbols are acquired by running the 'nm' program with parameters:\n";
    print "    @defined_symbols_parameters.\n";
    print "Undefined symbols are acquired by running the 'nm' program with parameters:\n";
    print "    @undefined_symbols_parameters.\n";
    print "\n";
    print "Usage: $0 [-h|--help|--print-json] [translation_unit...]\n";
    print "    -h\n";
    print "    --help\n";
    print "        Print this help message.\n";
    print "    --print-json\n";
    print "        Print translation units analysis in json format.\n";
}

sub defined_symbol_info {
    my ($address, $size, $type, $name) = split ' ', shift;
    my @scope = split '::', $name;
    return (
        "address" => "0x" . $address,
        "size" => "0x" . $size,
        "type" => $type,
        "fullname" => $name,
        "name" => pop @scope,
        "scope" => \@scope
    );
}

sub undefined_symbol_info {
    my ($type, $name) = split ' ', shift;
    my @scope = split '::', $name;
    return (
        "type" => $type,
        "fullname" => $name,
        "name" => pop @scope,
        "scope" => \@scope
    );
}

sub get_translation_unit_data {
    my $translation_unit_name = shift;
    return `"nm" @_ $translation_unit_name`;
}

sub parse_translation_unit_data {
    my $translation_unit_name = shift;
    my $symbol_creator = shift;
    return map +{ $symbol_creator->($_) }, get_translation_unit_data($translation_unit_name, @_);
}

sub create_translation_unit {
    my $translation_unit_name = shift;
    return (
        "defined" => [ parse_translation_unit_data($translation_unit_name, \&defined_symbol_info, @defined_symbols_parameters) ],
        "undefined" => [ parse_translation_unit_data($translation_unit_name, \&undefined_symbol_info, @undefined_symbols_parameters) ]
    );
}

my %g_symbol_locations;

sub initialize_translation_units {
    my $translation_units_ref = shift;
    for my $file_name (@_) {
        $translation_units_ref->{$file_name} = { create_translation_unit($file_name) };
    }
    for my $file_name (@_) {
        for my $symbol (@{ $translation_units_ref->{$file_name}{"defined"} }) {
            push @{ $g_symbol_locations{$symbol->{"fullname"}} }, $file_name; # store symbol location info
        }
    }
}

sub load_symbol_locations {
    my $translation_units_ref = shift;
    for my $key (keys %$translation_units_ref) {
        for my $undefined_symbols_ref ($translation_units_ref->{$key}{"undefined"}) {
            for my $undefined_symbol_ref (@$undefined_symbols_ref) {
                $undefined_symbol_ref->{"location"} = $g_symbol_locations{$undefined_symbol_ref->{"fullname"}};
                $undefined_symbol_ref->{"location"} ||= [];
            }
        }
    }
}

sub encode_json;

sub encode_json_string {
    my $item = shift;
    return "\"" . $item . "\"";
}

sub encode_json_primitive {
    return encode_json_string shift;
}

sub encode_json_array {
    my $array_ref = shift;
    my $encoded = "[";
    unless (!@$array_ref) {
        my $first_item = shift @$array_ref;
        $encoded .= encode_json($first_item);
        for my $item (@$array_ref) {
            $encoded .= "," . encode_json($item);
        }
    }
    $encoded .= "]";
    return $encoded;
}

sub encode_json_object {
    my $hash_ref = shift;
    my $encoded = "{";
    my @keys = keys %$hash_ref;
    unless (!@keys) {
        my $first_key = shift @keys;
        $encoded .= encode_json_string($first_key) . ":" . encode_json($hash_ref->{$first_key});
        for my $key (@keys) {
            $encoded .= "," . encode_json_string($key) . ":" . encode_json($hash_ref->{$key});
        }
    }
    $encoded .= "}";
    return $encoded;
}

sub encode_json {
    my $item = shift;
    switch (ref($item)) {
    case "ARRAY" { return encode_json_array($item) }
    case "HASH" { return encode_json_object($item) }
    else { return encode_json_primitive($item) }
    }
}

sub process_args {
    my %args;
    for my $arg (@ARGV) {
        switch($arg) {
        case ["-h", "--help"] { help_message(); exit(0); }
        case ["--print-json"] { $args{"print_json"} = 1; }
        else { push @{ $args{"translation_unit_files"} }, $arg; }
        }
    }
    return %args;
}

sub main {
    my %args = process_args();

    if (!$args{"translation_unit_files"}) {
        exit(0);
    }

    my %translation_units;

    initialize_translation_units(\%translation_units, @{ $args{"translation_unit_files"} });
    load_symbol_locations(\%translation_units);

    if ($args{"print_json"}) {
        print encode_json(\%translation_units), "\n";
    }
}

main();
